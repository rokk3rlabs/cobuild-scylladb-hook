var cassandra = require('cassandra-driver');
var uuid = require('uuid/v4');
var _  = require('lodash');
var compose = require('composeaddresstranslator');
var async = require('async');
var moment = require('moment');
var uuidParse = require('uuid-parse');

module.exports = function (credentials) {
	var authProvider = new cassandra.auth.PlainTextAuthProvider(credentials.username, credentials.password)
	if(credentials.useAddresses){
		translator = new compose.ComposeAddressTranslator();
		translator.setMap(credentials.addresses);

		client = new cassandra.Client({
		    contactPoints: credentials.contactPoints,
		    policies: {
	        	addressResolution: translator
	    	},
		    authProvider: authProvider,
		    sslOptions: true
		});
	}

	if(!credentials.useAddresses){
		client = new cassandra.Client({
		    contactPoints: credentials.contactPoints,
		    authProvider: authProvider,
		    sslOptions: true
		});
	}
	

	function createKeySpace(data){
		return new Promise((resolve, reject) => {

		
			var query = `CREATE KEYSPACE IF NOT EXISTS ${data.name} WITH replication ={'class': '${data.class}', 'replication_factor': '${data.replicationFactor}' }`

		    client.execute(query, function(err, results){

		    	if(err){
		    		reject(err);
		    	}
		    	resolve(results)
		    })
		})
	}

	function insert(table, data){

		return new Promise((resolve, reject) => {
			var id;
			var keys = Object.keys(data);

			if(_.has(data, 'id')){
				var params = _.map(keys, function(key){
				
					return data[key];
				})

			}else{
				keys.push('id');

				var params = _.map(keys, function(key){
					if(key == 'id'){
						id = uuid(); 
						return id;
					}
					return data[key];
				})
			}
			

			var replaceParams = _.map(params, function(param){
				return '?'
			})
			var query = `INSERT INTO ${credentials.keyspace}.${table} (${keys.toString()}) VALUES (${replaceParams.toString()})`
	
			client.execute(query, params, {prepare: true}, function(err, results){
				if(err){
					reject(err)
				}
				resolve(id)
			})
		});
		
	}

	function update(table, data){
		return new Promise((resolve, reject) => {
			var condition;

			var keys = Object.keys(data);


           	keys = _.remove(keys, function(key){
           		return key != 'id'
           	})


			var params = _.map(keys, function(key){
				return data[key];
			})

			params.push(data['id'])
	

			var replaceKeys = _.map(keys, function(key){
                return _.replace(key, key, key + ' = ?')
			})

			var query = `UPDATE ${credentials.keyspace}.${table} SET ${replaceKeys.toString()} WHERE id = ?`

			client.execute(query, params, {prepare: true}, function(err, results){
				if(err){
					reject(err)
				}
				resolve(results)
			})
		})
	}

	function find(table, id){
		return new Promise((resolve, reject) => {
			var data = [];
			var query = id ? `SELECT * FROM ${credentials.keyspace}.${table} WHERE id=${id}` : `SELECT * FROM ${credentials.keyspace}.${table}`
			client.eachRow(query ,null, { prepare : true , fetchSize : 1000000 }, function(err, results){
				data.push(results)
			}, function(err, results){
				if(err){
					reject(err)
				}
				resolve(data)
			})
		});
	}

	function createTableOrType(table, data){
		return new Promise((resolve, reject) => {
			var primaryKeys = [];
			var keys = Object.keys(data);

			var params = _.map(keys, function(key){

				if(_.includes(data[key].type, 'PRIMARY KEY')){

					primaryKeys.push(key);
					data[key].type = _.replace(data[key].type, 'PRIMARY KEY', '')

				}
				return key+ ' ' +data[key].type
			})


			if(_.lowerCase(table.type) == 'type'){
				var query = `CREATE ${table.type} IF NOT EXISTS ${credentials.keyspace}.${table.name} (${params})`
			}

			if(_.lowerCase(table.type) == 'table'){
				var query = `CREATE ${table.type} IF NOT EXISTS ${credentials.keyspace}.${table.name} (${params} , PRIMARY KEY (${primaryKeys}))`
			}

			client.execute(query, function(err, results){
				if(err){
					reject(err)
				}
				resolve(results)
			})
		})
	}

	function dropTableOrType(table, views, indexes){
		return new Promise((resolve, reject) => {

			var tasks = [];

			function preparateToRemoveViews(cb) {
				var self = this;
				async.each(views, function(view, cbEach){
					self.dropView(view)
					.then(function(response){
						cbEach(null, response);
					})
					.catch(function(err){
						cbEach(err)
					})
				}, function(err, response){
					if(err){
						return cb(err)
					}
					cb(null, response)
				})
			}

			function preparateToRemoveIndexes(cb) {
				var self = this;
				async.each(indexes, function(index, cbEach){
					self.dropIndex(index)
					.then(function(response){
						cbEach(null, response)
					})
					.catch(function(err){
						cbEach(err)
					})
				}, function(err, response){
					if(err){
						return cb(err)
					}
					cb(null, response)
				})
			}

			function drop(cb){
				var query = `DROP ${table.type} IF EXISTS ${credentials.keyspace}.${table.name}`
				client.execute(query, function(err, results){
					if(err){
						return cb(err);
					}
					cb(null, results);
				})
			}


			if(_.upperCase(table.type) == 'TABLE' && !_.isEmpty(views)){
				tasks.push(preparateToRemoveViews)
			}

			if(_.upperCase(table.type) == 'TABLE' && !_.isEmpty(indexes)){
				tasks.push(preparateToRemoveIndexes)
			}

			tasks.push(drop)

			async.waterfall(tasks, function(err, results){
				if(err)
					reject(err)

				resolve(results)
			})

			
		})
	}

	function dropKeySpace(keyspace){
		return new Promise((resolve, reject) => {
			var query = `DROP KEYSPACE IF EXISTS ${keyspace}`;
			client.execute(query, function(err, results){
				if(err){
					reject(err);
				}
				resolve(results);
			})

		});
	}


	function remove(table, id){
		return new Promise((resolve, reject) => {
			var query = `DELETE FROM ${credentials.keyspace}.${table} WHERE id = ?`
			var params = [id];
			client.execute(query, params,  {prepare: true}, function(err, results){
				if(err){
					reject(err);
				}
				resolve(results);
			})
		});
	}

	function insertMany(table, array){
		return new Promise((resolve, reject) => {
			var queries = [];
			async.each(array, function(data, cb){
				var id;
				var keys = Object.keys(data);

				if(_.has(data, 'id')){
					var params = _.map(keys, function(key){
					
						return data[key];
					})

				}else{
					keys.push('id');

					var params = _.map(keys, function(key){
						if(key == 'id'){
							id = uuid(); 
							return id;
						}
						return data[key];
					})
				}
				

				var replaceParams = _.map(params, function(param){
					return '?'
				})

				queries.push({
					query: `INSERT INTO ${credentials.keyspace}.${table} (${keys.toString()}) VALUES (${replaceParams.toString()})`,
					params: params
				});

				cb()

			}, function(errEach){

				if(errEach){
					reject(errEach);
				}
				queries = _.chunk(queries, 50);
				async.each(queries, function(queriesArray, cb){

					client.batch(queriesArray,  {prepare: true}, function(err, results){
						cb(err, results)
					});

				}, function(err, results){
					if(err){
						reject(err);
					}
					resolve(results);
				})
				
			})
			
		});
	}

	function createView(view, data){
		return new Promise((resolve, reject) => {
			if(_.isEmpty(data.conditions)){
				reject('Condtions cannot be blank');
			}

			if(!_.isEmpty(data.conditions) && _.isArray(data.conditions)){
				data.conditions = data.conditions.toString();
				data.conditions = replaceAll(data.conditions, ',',' AND ');
			}
			if(!_.isEmpty(data.columns) && _.isArray(data.columns)){
				data.columns = data.columns.toString();
			}

			if(_.isEmpty(data.columns)){
				data.columns = '*'
			}

			var query = `CREATE MATERIALIZED VIEW ${credentials.keyspace}.${view} AS SELECT ${data.columns} FROM ${data.table} WHERE ${data.conditions} PRIMARY KEY (${data.key}, id)`
			client.execute(query, function(err, results){
				if(err){
					reject(err);
				}
				resolve(results);
			})
		});
	}

	function createIndex(table, column){
		return new Promise((resolve, reject) => {
			var query = `CREATE INDEX ${table}_${column}_idx ON ${credentials.keyspace}.${table}(${column})`
			client.execute(query, function(err, results){
				if(err){
					reject(err);
				}
				resolve(results);
			})
		})
	}

	function dropIndex(index){
		return new Promise((resolve, reject) => {
			var query = `DROP INDEX IF EXISTS ${credentials.keyspace}.${index.name}`
			client.execute(query, function(err, results){
				if(err){
					reject(err);
				}
				resolve(results);
			})
		})
	}

	function customQuery(query){
		return new Promise((resolve, reject) => {
			client.execute(query, function(err, results){
				if(err){
					reject(err);
				}
				resolve(results);
			})
		});
	}


	function dropView(view){
		return new Promise((resolve, reject) => {
			var query = `DROP MATERIALIZED VIEW IF EXISTS ${credentials.keyspace}.${view.name}`
			client.execute(query, function(err, results){
				if(err){
					reject(err);
				}
				resolve(results);
			})
		})
	}

	function replaceAll(data, search, replacement){
		return data.split(search).join(replacement)
	}


	return {
		insert: insert,
		update: update,
		find: find,
		remove: remove,
		createTableOrType: createTableOrType,
		dropTableOrType: dropTableOrType,
		createKeySpace:createKeySpace,
		dropKeySpace:dropKeySpace,
		dropIndex:dropIndex,
		insertMany: insertMany,
		createView: createView,
		dropView: dropView,
		createIndex: createIndex,
		customQuery: customQuery
	}

}