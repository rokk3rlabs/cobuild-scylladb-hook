var should = require('should');

describe('Scyllab Hook', function(){
  var fixtures = {};

  before(function (done) {
      loadFixtures();
      done();
  });

  it('Should create keyspace', function (done) {
    var scyllabHook = require(process.cwd()+'/index.js')(fixtures.credentials);
    this.timeout(15000);

    scyllabHook.createKeySpace(fixtures.keyspace)
    .then(function(response){
      console.log(response)
      done();
    })
   
  });

  it('Should create a table or a type', function (done) {
    var scyllabHook = require(process.cwd()+'/index.js')(fixtures.credentials);
    this.timeout(15000);
    
    scyllabHook.createTableOrType(fixtures.table, fixtures.data)
    .then(function(response){
      console.log(response)
      done();
    })
   
  })
  
  it('Should insert', function (done) {
    var scyllabHook = require(process.cwd()+'/index.js')(fixtures.credentials);
    this.timeout(15000);

    scyllabHook.insert(fixtures.table.name, fixtures.insert)
    .then(function(response){
      console.log(response)
      done();
    })
   
  });

  it('Should update', function (done) {
    var scyllabHook = require(process.cwd()+'/index.js')(fixtures.credentials);
    this.timeout(15000);
    
    scyllabHook.update(fixtures.table.name, fixtures.update)
    .then(function(response){
      console.log(response)
      done();
    })
   
  });

  it('Should find', function (done) {
    var scyllabHook = require(process.cwd()+'/index.js')(fixtures.credentials);
    this.timeout(15000);
    
    scyllabHook.find(fixtures.table.name)
    .then(function(response){
      console.log(response)
      done();
    })
   
  })

  it('Should drop a table or a type', function (done) {
    var scyllabHook = require(process.cwd()+'/index.js')(fixtures.credentials);
    this.timeout(15000);
    
    scyllabHook.dropTableOrType(fixtures.table)
    .then(function(response){
      console.log(response)
      done();
    })
   
  })

  it('Should drop keyspace', function (done) {
    var scyllabHook = require(process.cwd()+'/index.js')(fixtures.credentials);
    this.timeout(15000);
    
    scyllabHook.dropKeySpace(fixtures.keyspace.name)
    .then(function(response){
      console.log(response)
      done();
    })
   
  })

  it('Should delete', function (done) {
    var scyllabHook = require(process.cwd()+'/index.js')(fixtures.credentials);
    this.timeout(15000);
    
    scyllabHook.remove(fixtures.table.name, fixtures.update.id)
    .then(function(response){
      console.log(response)
      done();
    })
   
  })

  function loadFixtures(argument) {

    var Cobuild = require('cobuild2');
    var path = require('path');

    Cobuild.Utils.Files.listFilesInFolder(__dirname + '/fixtures')
    .forEach(function (file) {
        var completePath =  path.join(__dirname , 'fixtures',  file)
        fixtures[file.slice(0, -3)] = require(completePath);
        
    });
  }
});